module gitlab.com/k54/cog

go 1.16

require (
	github.com/valyala/bytebufferpool v1.0.0
	gitlab.com/k54/expect v0.0.0-20210510174107-eca9c6ed21a4
)

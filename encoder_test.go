package cog

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io"
	"math"
	"testing"
	"time"

	"gitlab.com/k54/expect"
)

// awful test to write.
func TestEncode(t *testing.T) {
	defer expect.Cleanup(t)
	stamp := time.Unix(989539200, 0)
	enc := getEncoder()
	defer enc.Release()
	enc.Object("", func(Encoder) {
		enc.Value("value", 42)
		enc.String("string", "value")
		enc.RawString("rawstring", "rawvalue")
		enc.Stringer("stringer", 3*time.Second)
		enc.Err("err", io.EOF)
		enc.StringBytes("stringbytes", []byte("bytesvalue"))
		enc.Nil("nil")
		enc.Bool("bool", true)
		enc.Time("time", stamp, time.RFC3339Nano)
		enc.Timestamp("timestamp", stamp)
		enc.Bytes("bytes", []byte("bytesvalue"))
		enc.Int("int", 666)
		enc.Int64("int64", -40_000_000)
		enc.Uint64("uint64", 123_456_789)
		enc.Float("float", 2.718281828)
		enc.IntSlice("intslice", []int{1, 2, 3})
		enc.BoolSlice("boolslice", []bool{true, true, false})
		enc.Int64Slice("int64slice", []int64{-5, -2})
		enc.Uint64Slice("uint64slice", []uint64{12, 42, 69, 420})
		enc.FloatSlice("floatslice", []float64{math.Pi, math.Sqrt2, math.SmallestNonzeroFloat64})
		enc.StringSlice("stringslice", []string{"never", "gonna", "give", "you", "up"})
		mp := map[string]int{
			"three":    3,
			"nine":     9,
			"negative": -27,
		}
		enc.Object("objectfunc", func(e Encoder) {
			for k, v := range mp {
				e.Int(k, v)
			}
		})
	})

	buf := bytes.Buffer{}
	_, err := enc.enc.WriteTo(&buf)
	t.Log(err)
	expect.Nil(err)

	out := map[string]interface{}{}
	err = json.Unmarshal(buf.Bytes(), &out)
	expect.Nil(err)

	compare := map[string]interface{}{
		"value":       42.0, // special case, JSON doesn't handle integers
		"string":      "value",
		"rawstring":   "rawvalue",
		"stringer":    (3 * time.Second).String(),
		"err":         io.EOF.Error(),
		"stringbytes": "bytesvalue",
		"nil":         nil,
		"bool":        true,
		"time":        stamp.Format(time.RFC3339Nano),
		"timestamp":   float64(stamp.Unix()),
		"bytes":       base64.StdEncoding.EncodeToString([]byte("bytesvalue")),
		"int":         666.0,
		"int64":       -40_000_000.0,
		"uint64":      123_456_789.0,
		"float":       2.718281828,

		"intslice":    []interface{}{1.0, 2.0, 3.0},
		"boolslice":   []interface{}{true, true, false},
		"int64slice":  []interface{}{-5.0, -2.0},
		"uint64slice": []interface{}{12.0, 42.0, 69.0, 420.0},
		"floatslice":  []interface{}{math.Pi, math.Sqrt2, math.SmallestNonzeroFloat64},
		"stringslice": []interface{}{"never", "gonna", "give", "you", "up"},

		"objectfunc": map[string]interface{}{
			"three":    3.0,
			"nine":     9.0,
			"negative": -27.0,
		},
	}
	expect.DeepEqual(out, compare)
}

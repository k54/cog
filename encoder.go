package cog

import (
	"fmt"
	"time"

	"github.com/valyala/bytebufferpool"
	"gitlab.com/k54/cog/pkg/medea"
	"gitlab.com/k54/cog/pkg/notsafe"
)

// Encoder is the type on which you encode all your context values.
type Encoder struct {
	enc *medea.JSONEncoder
}

// getEncoder borrows an Encoder from an internal pool, don't forget to Release() thereafter.
func getEncoder() Encoder {
	return Encoder{enc: medea.GetEncoder()}
}

func (e Encoder) Release() {
	e.enc.Release()
}

// Value encodes value as a valid sub value
// It is quite slow as it doesnt know the type at compile time
// Useful for debugging.
func (e Encoder) Value(key string, value interface{}) {
	e.enc.Value(key, value)
}

// String escapes only what is mandated by RFC 8259.
func (e Encoder) String(key, value string) {
	e.enc.String(key, value)
}

// RawString has no escaping at all, not even quotes for JSON. Use String in most cases (RawString is faster though).
func (e Encoder) RawString(key, value string) {
	e.enc.RawString(key, value)
}

// Stringer is a short form of String(key, value.String()).
func (e Encoder) Stringer(key string, value fmt.Stringer) {
	e.enc.String(key, value.String())
}

// Err is a short form of String(key, value.Error()).
func (e Encoder) Err(key string, value error) {
	e.enc.String(key, value.Error())
}

// StringBytes is similar to RawString(key, string(value)), but doesn't allocate.
func (e Encoder) StringBytes(key string, value []byte) {
	e.enc.StringBytes(key, value)
}

// Nil encodes absence of a value, i.e. JSON token `null`.
func (e Encoder) Nil(key string) {
	e.enc.Nil(key)
}

// Bool encodes value as a boolean, i.e. JSON token `true` or `false`.
func (e Encoder) Bool(key string, value bool) {
	e.enc.Bool(key, value)
}

// TimeFormat encodes the time with layout as (time.Time).Format.
func (e Encoder) Time(key string, value time.Time, layout string) {
	buf := bytebufferpool.Get()
	buf.B = value.AppendFormat(buf.B, layout)
	e.enc.String(key, notsafe.String(buf.B))
	bytebufferpool.Put(buf)
}

// Timestamp encodes the time as a UNIX timestamp (second).
func (e Encoder) Timestamp(key string, value time.Time) {
	e.enc.Int64(key, value.Unix())
}

// Bytes writes values as base64 string.
func (e Encoder) Bytes(key string, values []byte) {
	e.enc.Bytes(key, values)
}

// Int encodes value as a number.
func (e Encoder) Int(key string, value int) {
	e.enc.Int(key, value)
}

// Int64 encodes value as a number.
func (e Encoder) Int64(key string, value int64) {
	e.enc.Int64(key, value)
}

// Uint64 encodes value as a number.
func (e Encoder) Uint64(key string, value uint64) {
	e.enc.Uint64(key, value)
}

// Float encodes value as a number, it is ~3x slower than Int64.
func (e Encoder) Float(key string, value float64) {
	e.enc.Float(key, value)
}

// IntSlice encodes values as an array of the contents.
func (e Encoder) IntSlice(key string, values []int) {
	e.Array(key, func(enc Encoder) {
		for _, v := range values {
			enc.Int("", v)
		}
	})
}

// BoolSlice encodes values as an array of the contents.
func (e Encoder) BoolSlice(key string, values []bool) {
	e.Array(key, func(enc Encoder) {
		for _, v := range values {
			enc.Bool("", v)
		}
	})
}

// Int64Slice encodes values as an array of the contents.
func (e Encoder) Int64Slice(key string, values []int64) {
	e.Array(key, func(enc Encoder) {
		for _, v := range values {
			enc.Int64("", v)
		}
	})
}

// Uint64Slice encodes values as an array of the contents.
func (e Encoder) Uint64Slice(key string, values []uint64) {
	e.Array(key, func(enc Encoder) {
		for _, v := range values {
			enc.Uint64("", v)
		}
	})
}

// FloatSlice encodes values as an array of the contents.
func (e Encoder) FloatSlice(key string, values []float64) {
	e.Array(key, func(enc Encoder) {
		for _, v := range values {
			enc.Float("", v)
		}
	})
}

// StringSlice encodes values as an array of the contents.
func (e Encoder) StringSlice(key string, values []string) {
	e.Array(key, func(enc Encoder) {
		for _, v := range values {
			enc.String("", v)
		}
	})
}

// Array encases fn in an array
//
// Example:
//   e.Array(key, func(enc medea.Encoder) {
//   	for _, v := range mySlice {
//   		enc.String("", v.Name)
//   	}
//   })
func (e Encoder) Array(key string, fn func(Encoder)) {
	e.enc.Key(key)
	e.enc.AppendByte('[')
	fn(e)
	e.enc.ReplaceLast(']')
	e.enc.AppendByte(',')
}

// Object encases fn in an object
//
// Example:
//   e.Object(key, func(enc medea.Encoder) {
//   	for k, v := range myMap {
//   		enc.String(k, v.Name)
//   	}
//   })
func (e Encoder) Object(key string, fn func(Encoder)) {
	e.enc.Key(key)
	e.enc.AppendByte('{')
	fn(e)
	e.enc.ReplaceLast('}')
	e.enc.AppendByte(',')
}

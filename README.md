# ⚙️ cog [![GoDoc][doc-img]][doc]

Extremely fast structured and leveled logger for golang, now with most of the code test-covered ! (albeit pretty bad tests)

## Installation

`go get -u gitlab.com/k54/cog`

## Very Quick Start

```go
log := cog.New(cog.Stderr, cog.Options{
    Level:     cog.DebugLevel,
})
defer log.Sync()

log.Debug("i'm here")
// {"message":"i'm here","level":"debug"}

log.Debug("failed to fetch URL", func(enc cog.Encoder) {
    enc.String("url", url)
    enc.Int("attempt", 3)
    enc.Stringer("backoff", time.Second)
})
// {"message":"failed to fetch URL","level":"debug","url":"https://google.com","attempt":3,"backoff":"1s"}
```


## Quick Start

```go
log := cog.New(cog.Stderr, cog.Options{
    Level:     cog.DebugLevel,
    CallerKey: "caller",
    TimeKey:   "time",
})
defer log.Sync()
const url = "https://example.com"

log.Debug("failed to fetch URL", func(enc cog.Encoder) {
    enc.String("url", url)
    enc.Int("attempt", 3)
    enc.Stringer("backoff", time.Second)
})

log.Debug("something wrong happened")

err := json.Unmarshal(..., &config)
log.Fatal(err) // doesnt do anything if err == nil, saves you from writing if err != nil {...}
```

Output (beautified):

```json
{
    "message": "failed to fetch URL",
    "level": "debug",
    "time": 1599728064798211000,
    "caller": "cog/cog_test.go:24",
    "url": "https://google.com",
    "attempt": 3,
    "backoff": "1s"
}
```

<!-- See the [documentation][doc] and [FAQ](FAQ.md) for more details. -->

## Implementation

This package is implemented with a fast, 0 allocations JSON encoder, and two internal logging stacks:
The debug, and the standard stacks.

The standard stack is just what you would expect in a logging library, and serves the Info and Error levels.

The debug stack is a little bit different, as it is optimized for debugging when the BetterDebug option is present.

## Motivation

As pioneered by zap and zerolog, this package was created to support the need for high-performance, low-allocation and structured logging.

Some API niceties are also supported, such as `log.Error(err)` which logs only if err != nil.

Only JSON is supported, as this package is backed by an extremely fast and 0-allocation JSON encoder.

The API is pretty similar to onelog, but is a bit less heavy in my opinion (no Warn/WarnWith/WarnWithFields madness).

The decision to have your fields in a func makes them lazy-evaluated, so you won't compute fields nor encode them on a disabled logger, as you can see in the benchmarks below.

Although the syntax is pretty heavy, it's not that bad for this performance gain.

Cog supports a global Logger context that is cached at logger creation.

```go
log := cog.New(cog.Stderr, cog.Options{
    Level:     cog.DebugLevel,
    CallerKey: "caller",
    TimeKey:   "time",
    Context: func(enc cog.Encoder) {
        enc.String("app", "api")
    },
})
defer log.Sync()

log.Debug("failed to fetch URL", func(enc cog.Encoder) {
    enc.String("url", url)
    enc.Int("attempt", 3)
    enc.Stringer("backoff", time.Second)
})
```

```json
{
    "message": "failed to fetch URL",
    "level": "debug",
    "time": 1599728064798211000,
    "app": "api",
    "caller": "cog/cog_test.go:24",
    "url": "https://google.com",
    "attempt": 3,
    "backoff": "1s"
}
```

## Benchmarks

Run on cpu: Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz (4c/8t)

Date: 2021-04-21, go1.16

Log a static message to a disabled logger level (no context)

| Package | Time | Time% to cog |
| :------ | :--: | :----------: |
| ⚙️ cog | 0.3823 ns/op | +0%
| ⚡ zap | 2.277 ns/op | +495%
| rs/zerolog | 1.058 ns/op | +176%
| francoispqt/onelog | 0.6566 ns/op | +71%


Log a message to a disabled logger level with static context

| Package | Time | Time% to cog |
| :------ | :--: | :----------: |
| ⚙️ cog | 0.3642 ns/op | +0%
| ⚡ zap | 2.435 ns/op | +568%
| rs/zerolog | 1.069 ns/op | +193%
| francoispqt/onelog | 0.6525 ns/op | +79%


Log a message to a disabled logger level with dynamic context

| Package | Time | Time% to cog |
| :------ | :--: | :----------: |
| ⚙️ cog | 0.8116 ns/op | +0%
| ⚡ zap | 177.8 ns/op | +21807%
| rs/zerolog | 66.15 ns/op | +8050%
| francoispqt/onelog | 1.349 ns/op | +66%


Log a static message (no context)

| Package | Time | Time% to cog |
| :------ | :--: | :----------: |
| ⚙️ cog | 105.6 ns/op | +0%
| ⚡ zap | 236.7 ns/op | +124%
| rs/zerolog | 163.7 ns/op | +54%
| francoispqt/onelog | 91.55 ns/op | -13%


Log a message with static context

| Package | Time | Time% to cog |
| :------ | :--: | :----------: |
| ⚙️ cog | 88.90 ns/op | +0%
| ⚡ zap | 236.0 ns/op | +165%
| rs/zerolog | 176.7 ns/op | +98%
| francoispqt/onelog | 1072 ns/op | +1105%


Log a message with dynamic context

| Package | Time | Time% to cog |
| :------ | :--: | :----------: |
| ⚙️ cog | 841.0 ns/op | +0%
| ⚡ zap | 2234 ns/op | +165%
| rs/zerolog | 7211 ns/op | +757%
| francoispqt/onelog | 1020 ns/op | +21%

As you can see, Cog is way faster than zap and zerolog in every benchmarked situation.
If you can create a somewhat realistic benchmark where this isn't the case, please file an issue.
The disabled logger performance is so good you can leave your debug calls in production mode.

Onelong has comparable performance (except for static context) and is more stable, so you should probably use it if you find its API to be better.

## Considerations

While an `enc.Value` method is available, it's a lot slower than calling the correct encoder method.

For example, `enc.Value("", duration)` is ~3-4 times slower than `enc.Stringer("", duration)`.

You also don't get type safety, so the encoder could resort to reflection, which is extremely slow.
The `enc.Value` is still very useful for debugging, but should not be used in any kind of loop or resource-heavy task.

It's implemented as a best-effort algorithm to use internal methods, but falls back to `encoding/json`.

The log timestamp is a simple unixNano timesteamp, because marshalling a `time.Time` to a standard format, say RFC3339Nano, is 20x slower. Yes it is extremely slow (~400ns vs ~20ns).
This is while including `time.Now()` in the benchmark loop.

Is is still extremely useful especially for debugging, so a `BetterDebug` option is provided so that a RFC3339 timestamp is output instead while debugging.

In case you want to slow down your logs, but have them more human-readable, a `TimeFormat` options is provided. Cog will still be faster than zap or zerolog though.


## Development Status: Unstable

Breaking changes can and will happen anytime

I will try to make cog somewhat stable, but don't expect any stability from `/pkg/`

<!-- Released under the [MIT License](LICENSE.txt). -->

[doc-img]: https://godoc.org/gitlab.com/k54/cog?status.svg
[doc]: https://godoc.org/gitlab.com/k54/cog

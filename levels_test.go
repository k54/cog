package cog_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"testing"

	"gitlab.com/k54/cog"
	"gitlab.com/k54/expect"
)

func TestLoggingErrNil(t *testing.T) {
	tw := new(testWriter)
	log := cog.New("test", cog.Options{
		Target: cog.Writer(tw),
		Level:  cog.InfoLevel,
	})

	log.Panic(nil)
	if *tw {
		t.Fatal("nil error should not have written")
	}
	log.Error(nil)
	if *tw {
		t.Fatal("nil error should not have written")
	}
}

var errStub = errors.New("stub error")

func TestLoggingLeveled(t *testing.T) {
	defer expect.Cleanup(t)
	tw := new(testWriter)
	log := cog.New("test", cog.Options{
		Target: cog.Writer(tw),
		Level:  cog.ErrorLevel,
	})

	tw.Reset()
	log.Error(errStub)
	expect.If(!tw.WrittenTo(), "log.Error should have logged at Info level")

	tw.Reset()
	log.Info("test")
	expect.If(tw.WrittenTo(), "log.Info should not have logged at Info level")

	tw.Reset()
	log.Debug("test")
	expect.If(tw.WrittenTo(), "log.Debug should not have logged at Info level")
}

func Mock() (*cog.Logger, *bytes.Buffer) {
	buf := new(bytes.Buffer)
	l := cog.New("test", cog.Options{
		Target: cog.Writer(buf),
		Level:  cog.DebugLevel,
	})
	return l, buf
}

func TestLogger_Panic(t *testing.T) {
	defer expect.Cleanup(t)
	expect.Panic(func() {
		l := cog.New("test", cog.Options{
			Target: cog.Writer(io.Discard),
		})
		l.Panic(errStub)
	})
}

func TestLogger_Printf(t *testing.T) {
	l, buf := Mock()
	l.Printf("%d is 3", 3)
	out := map[string]string{}
	err := json.Unmarshal(buf.Bytes(), &out)
	if err != nil {
		t.Error(err)
	}
	if out["message"] != "3 is 3" {
		t.Error("formatting did not work")
	}
}

func TestLogLevel_String(t *testing.T) {
	tests := []struct {
		name string
		l    cog.LogLevel
		want string
	}{
		{"Level.String", cog.ErrorLevel, "error"},
		{"Level.String", cog.InfoLevel, "info"},
		{"Level.String", cog.DebugLevel, "debug"},
		{"Level.String", cog.All, ""},
		{"Level.String", cog.Disabled, ""},
		{"Level.String", cog.LogLevel(-12), ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.l.String(); got != tt.want {
				t.Errorf("LogLevel.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLevelOf(t *testing.T) {
	type args struct {
		in string
	}
	tests := []struct {
		name string
		args args
		want cog.LogLevel
	}{
		{"LevelOf", args{"error"}, cog.ErrorLevel},
		{"LevelOf", args{"info"}, cog.InfoLevel},
		{"LevelOf", args{"debug"}, cog.DebugLevel},

		{"LevelOf", args{"DEBUG"}, cog.DebugLevel},
		{"LevelOf", args{"INVALID VALUE"}, cog.InfoLevel},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := cog.LevelOf(tt.args.in); got != tt.want {
				t.Errorf("LevelOf() = %v, want %v", got, tt.want)
			}
		})
	}
}

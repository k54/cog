package cog

import "io"

// Default returns a ready-to-use *Logger, writing on Stderr with Debug level.
func Default() *Logger {
	return New("default", Options{
		Level: DebugLevel,
	})
}

// Context represents the context in which the log was output.
type Context func(Encoder)

// Options is the Logger options, passed to New()
// An empty *Key value means the corresponding context WILL NOT be output.
type Options struct {
	Target     Target      // Target on which to output the logs, defaults to Stderr
	Level      LogLevel    // LogLevel of the Logger, defaults to ErrorLevel
	LevelKey   string      // Key to use for logging LogLevel, defaults to "level" if empty
	NameKey    string      // Key to use for logging Name, defaults to "name" if empty
	CallerKey  string      // Key to use to print caller (file, line, function name), big perforance hit
	TimeKey    string      // Key to use for logging timestamp
	TimeFormat string      // Format to use, uses a simple but fast UnixNano timestamp if empty
	Context    Context     // Static context to log alongside other fields
	OnError    func(error) // Called when internal write error happened
	// When true, TimeFormat defaults to as RFC3339, and caller is always on for the Debug stack
	BetterDebug bool
}

// New creates a new *Logger for usage.
func New(name string, opts Options) *Logger {
	if opts.LevelKey == "" {
		opts.LevelKey = "level"
	}
	if opts.NameKey == "" {
		opts.NameKey = "name"
	}
	target := opts.Target
	if target == nil {
		target = Stderr()
	}
	l := &Logger{
		name: name,
		t:    target,
		opts: opts,
	}
	// populate l.cached
	enc := getEncoder()
	enc.String(opts.NameKey, name)
	if opts.Context != nil {
		opts.Context(enc)
	}
	l.cached = enc.enc.Cache()
	return l
}

func writeAll(buf []byte, w io.Writer) (n int64, err error) {
	if len(buf) == 0 {
		return
	}
	if nBytes := len(buf); nBytes > 0 {
		m, e := w.Write(buf)
		if m > nBytes {
			panic("medea.Encoder.WriteTo: invalid Write count")
		}

		n = int64(m)
		if e != nil {
			return n, e
		}

		// all bytes should have been written, by definition of
		// Write method in io.Writer
		if m != nBytes {
			return n, io.ErrShortWrite
		}
	}

	return n, nil
}

/*
Package cog implements a fast structured and leveled logger.

Only JSON is supported and the syntax is a bit heavy, but it's still reasonable.

The tradeoff for this heavy syntax is extreme performance, as a disabled logger
call only takes a single CPU cycle (if my measurements are correct).

	log := cog.Default()
	defer log.Sync()

	log.Warn("failed to fetch URL", func(enc cog.Encoder) {
		enc.String("url", "http://example.com")
		enc.Int("attempt", 3)
		enc.Stringer("backoff", time.Second)
	})

*/

package cog

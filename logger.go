package cog

import (
	"time"

	"gitlab.com/k54/cog/pkg/notsafe"
)

// Logger represents the logger
// An empty value is INVALID.
type Logger struct {
	t      Target
	opts   Options
	name   string
	cached []byte
}

// Sync calls the underlying Target's Sync method, flushing any buffered log entries.
// Applications should take care to call Sync before exiting.
func (l *Logger) Sync() error { return l.t.Sync() }

// inlines log, and does actual work in _log.
func (l *Logger) log(lvl LogLevel, message string, fields ...Context) {
	if l.opts.Level < lvl {
		return
	}
	l._log(lvl, message, fields...)
}

//go:noinline
func (l *Logger) _log(lvl LogLevel, message string, fields ...Context) {
	enc := getEncoder()

	enc.enc.AppendByte('{')
	{
		enc.String(l.opts.LevelKey, lvl.String())
		if l.opts.TimeKey != "" {
			if l.opts.TimeFormat != "" {
				enc.String(l.opts.TimeKey, time.Now().Format(l.opts.TimeFormat))
			} else {
				// time.Now().UnixNano is optimized to a single function call, so very cheap
				enc.Int64(l.opts.TimeKey, time.Now().UnixNano())
			}
		}
		if l.opts.CallerKey != "" {
			// extremely costly (comparatively)
			const skipFrames = 3 // _log, log, Log
			caller := notsafe.CallerName(skipFrames)
			enc.String(l.opts.CallerKey, caller)
		}

		enc.String("message", message)
		for _, field := range fields {
			if field != nil {
				field(enc)
			}
		}
		enc.enc.AppendByte(',')
		enc.enc.WriteCached(l.cached)
	}
	// enc.enc.Buf[len(enc.enc.Buf)-1] = '}'
	enc.enc.ReplaceLast('}')
	// enc.enc.AppendString("\n")
	enc.enc.AppendByte('\n')
	// enc.enc.AppendByte(',')
	// _, err := l.t.Write(enc.enc.Buf)
	_, err := enc.enc.WriteTo(l.t) //nolint:ifshort // false positive, we want to Release()
	writeAll(enc.enc.Buf, l.t)
	enc.Release()
	if err != nil && l.opts.OnError != nil {
		l.opts.OnError(err)
	}
}

// copied from _log and log, but adapted for BetterDebug option
// it is kept separate for performance reasons.
func (l *Logger) logDebug(message string, fields ...Context) {
	if l.opts.Level < DebugLevel {
		return
	}
	l._logDebug(message, fields...)
}

func (l *Logger) _logDebug(message string, fields ...Context) {
	now := time.Now()
	var caller string

	enc := getEncoder()

	if l.opts.CallerKey != "" || l.opts.BetterDebug {
		// extremely costly (comparatively)
		const skipFrames = 3 // _log, log, Log
		caller = notsafe.CallerName(skipFrames)
	}

	enc.Object("", func(_ Encoder) {
		if l.opts.LevelKey != "" {
			enc.Stringer(l.opts.LevelKey, DebugLevel)
		}
		if l.opts.TimeKey != "" {
			switch {
			case l.opts.TimeFormat != "":
				enc.Time(l.opts.TimeKey, now, l.opts.TimeFormat)
			case l.opts.BetterDebug:
				enc.Time(l.opts.TimeKey, now, time.RFC3339Nano)
			default:
				enc.Int64(l.opts.TimeKey, now.UnixNano())
			}
		} else if l.opts.BetterDebug {
			enc.Time("time", now, time.RFC3339Nano)
		}

		if l.opts.CallerKey != "" {
			enc.String(l.opts.CallerKey, caller)
		} else if l.opts.BetterDebug {
			enc.String("caller", caller)
		}

		enc.enc.WriteCached(l.cached)

		enc.String("message", message)
		for _, field := range fields {
			if field != nil {
				field(enc)
			}
		}
	})

	enc.enc.AppendByte('\n')
	_, err := enc.enc.WriteTo(l.t) //nolint:ifshort // false positive, we want to Release()
	enc.Release()
	if err != nil && l.opts.OnError != nil {
		l.opts.OnError(err)
	}
}

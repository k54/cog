package cog

import (
	"io"
	"os"
	"sync"
)

// Stderr is the equivalent of Writer(os.Stderr).
func Stderr() Target { return stderr }

// Stdout is the equivalent of Writer(os.Stdout).
func Stdout() Target { return stdout }

var (
	stdout Target = Writer(os.Stdout)
	stderr Target = Writer(os.Stderr)
)

// Target is an io.Writer that can also flush any buffered data, and is safe for concurrent use.
type Target interface {
	io.Writer
	sync.Locker
	Sync() error
}

type writeSyncer interface {
	io.Writer
	Sync() error
}

type writeLocker interface {
	io.Writer
	sync.Locker
}

type noopSync struct {
	writeLocker
}

func (w noopSync) Sync() error {
	return nil
}

type lockedWriter struct {
	sync.Mutex
	ws writeSyncer
}

func (s *lockedWriter) Write(bs []byte) (int, error) {
	s.Lock()
	n, err := s.ws.Write(bs)
	s.Unlock()
	return n, err
}

func (s *lockedWriter) Sync() error {
	s.Lock()
	err := s.ws.Sync()
	s.Unlock()
	return err
}

type writer struct {
	sync.Mutex
	w io.Writer
}

func (s *writer) Write(bs []byte) (int, error) {
	s.Lock()
	n, err := s.w.Write(bs)
	s.Unlock()
	return n, err
}

func (s *writer) Sync() error {
	return nil
}

// Writer wraps an io.Writer with a mutex to make it safe for concurrent use.
// It also adds a no-op Sync method if none exist on the io.Writer.
// In particular, *os.Files get locked, but they have a Sync method already.
func Writer(w io.Writer) Target {
	if w == nil {
		return nil
	}
	switch ws := w.(type) {
	case Target:
		return ws
	case writeLocker:
		return &noopSync{ws}
	case writeSyncer:
		return &lockedWriter{ws: ws}
	default:
		return &writer{w: w}
	}
}

var osOpenFile = os.OpenFile

// MustFile opens a file in write-only|append|create mode, calls Writer on it, and panics if error happened.
func MustFile(path string) Target {
	file, err := osOpenFile(path, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	return Writer(file)
}

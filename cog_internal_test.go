package cog

import (
	"testing"
)

type discardSync struct{}

func (discardSync) Write(b []byte) (n int, err error) {
	return len(b), nil
}
func (discardSync) Sync() error { return nil }
func (discardSync) Lock()       {}
func (discardSync) Unlock()     {}

func BenchmarkLog1(b *testing.B) {
	log := New("cog internal test", Options{
		Target: discardSync{},
	})
	for i := 0; i < b.N; i++ {
		log._log(InfoLevel, "message", func(e Encoder) {
			e.String("key", "value")
		})
	}
}

package cog_test

import (
	"io"
	"net/http"
	"testing"
	"time"

	"gitlab.com/k54/cog"
)

type discardSync struct{}

func (discardSync) Write(b []byte) (n int, err error) {
	return len(b), nil
}
func (discardSync) Sync() error { return nil }
func (discardSync) Lock()       {}
func (discardSync) Unlock()     {}

func getLogger() *cog.Logger {
	log := cog.New("cog_test", cog.Options{
		Target:      discardSync{},
		Level:       cog.All,
		BetterDebug: true,
		TimeKey:     "time",
		CallerKey:   "caller",
	})
	return log
}

func BenchmarkLogDebug(b *testing.B) {
	log := getLogger()
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			log.Debug("hey", func(e cog.Encoder) { e.Int("value", 42) })
		}
	})
}

func BenchmarkLogInfo(b *testing.B) {
	log := getLogger()
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			log.Info("hey", func(e cog.Encoder) { e.Int("value", 42) })
		}
	})
}

func BenchmarkLogError(b *testing.B) {
	log := getLogger()
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			log.Error(io.EOF, func(e cog.Encoder) { e.Int("value", 42) })
		}
	})
}

func BenchmarkLogDump(b *testing.B) {
	log := getLogger()

	b.RunParallel(func(p *testing.PB) {
		v := &http.Client{
			Timeout: 10 * time.Second,
		}
		for p.Next() {
			log.Dump(v)
		}
	})
}

package cog

import (
	"fmt"
	"strings"
)

// // Log outputs to the Logger with the appropriate LogLevel.
// //
// // Does not do fancy stuff like errors nil checks etc.
// func (l *Logger) Log(level LogLevel, message string, fields ...Context) {
// 	l.log(level, message, fields...)
// }

// Panic logs err at ErrorLevel, and then panics with err
//
// If err is nil, doesn't log nor panics.
//
// You can use for application initialization, for example in config loading:
//   conf, err := loadConfig()
//   log.Panic(err)
//   // now conf is valid
func (l *Logger) Panic(err error, fields ...Context) {
	if err == nil {
		return
	}
	l.log(ErrorLevel, err.Error(), fields...)
	panic(err)
}

// Error logs err at ErrorLevel
//
// If err is nil, doesn't log, so you can log.Error(file.Close()) and it won't
// log if there is no error closing the file.
func (l *Logger) Error(err error, fields ...Context) {
	if err == nil {
		return
	}
	l.log(ErrorLevel, err.Error(), fields...)
}

// Info logs message and fields at InfoLevel
func (l *Logger) Info(message string, fields ...Context) { l.log(InfoLevel, message, fields...) }

// Debug logs message (and fields) at DebugLevel, and honors BetterDebug
//
// Do NOT log sensitive info, even in debug level.
func (l *Logger) Debug(message string, fields ...Context) { l.logDebug(message, fields...) }

// Dump dumps the content of v with fmt.Printf("%+v", v) onto the debug stack (honoring BetterDebug).
func (l *Logger) Dump(v interface{}) { l.logDebug(fmt.Sprintf("%+v", v)) }

// Printf is a helper, logs to InfoLevel with message as returned by fmt.Sprintf,
// meant to be used by external packages that only know of this basic interface
//
// Is very slow and unhelpul compared to other methods.
func (l *Logger) Printf(format string, args ...interface{}) {
	l.log(InfoLevel, fmt.Sprintf(format, args...))
}

// LogLevel represents a logging level.
// If a *Logger is at ErrorLevel, a log.Warn WILL NOT be output.
// If a *Logger is at InfoLevel, a log.Info WILL be output.
//
// Basic recommendations:
//   fatal: wake the sysadmin up in the middle of the night
//   panic: log them (deduplicated) to the devops slack/discord/teams channel
//   error: periodically log their count to the dev channel, make them easily accessible
//   info:  periodically log their count to the dev channel
//   debug: disabled in production
type LogLevel int8

const (
	// Error: problem happened.
	ErrorLevel LogLevel = iota - 1
	// Informational: informational messages (lots of packets are dropping, system is slower than usual).
	InfoLevel
	// Debug: debug-level messages, should be disabled in production.
	DebugLevel

	// All levels enabled.
	All LogLevel = 127
	// Disables the logger.
	Disabled LogLevel = -128
)

// String implements fmt.Stringer.
func (l LogLevel) String() string {
	// way faster than map lookup here
	switch l {
	case ErrorLevel:
		return "error"
	case InfoLevel:
		return "info"
	case DebugLevel:
		return "debug"
	case All, Disabled:
		return ""
	}
	return ""
}

// LevelOf returns the LogLevel corresponding to the input
// Returns InfoLevel on invalid input.
func LevelOf(in string) LogLevel {
	switch strings.ToLower(in) {
	case "error":
		return ErrorLevel
	case "info":
		return InfoLevel
	case "debug":
		return DebugLevel

	default:
		return InfoLevel
	}
}

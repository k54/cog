package cog_test

import (
	"bytes"
	"io"

	// "os".
	"encoding/json"
	"errors"
	"io/ioutil"
	"strings"
	"testing"
	"time"

	"gitlab.com/k54/cog"
	"gitlab.com/k54/expect"
)

func TestReadme(t *testing.T) {
	log := cog.New("readme", cog.Options{
		Target: cog.Writer(io.Discard),
	})

	//nolint:errcheck // dont care about Sync error
	defer log.Sync()

	log.Debug("i'm here")

	log.Debug("failed to fetch URL", func(enc cog.Encoder) {
		enc.String("url", "https://google.com")
		enc.Int("attempt", 3)
		enc.Stringer("backoff", time.Second)
	})

	log = cog.New("readme with caller", cog.Options{
		Level:     cog.DebugLevel,
		CallerKey: "caller",
		TimeKey:   "time",
	})

	log.Debug("failed to fetch URL", func(enc cog.Encoder) {
		enc.String("url", "https://google.com")
		enc.Int("attempt", 3)
		enc.Stringer("backoff", time.Second)
	})

	log = cog.New("readme with context", cog.Options{
		Level:     cog.DebugLevel,
		CallerKey: "caller",
		TimeKey:   "time",
		Context: func(enc cog.Encoder) {
			enc.String("app", "api")
		},
	})

	log.Debug("failed to fetch URL", func(enc cog.Encoder) {
		enc.String("url", "https://google.com")
		enc.Int("attempt", 3)
		enc.Stringer("backoff", time.Second)
	})
}

func TestTimeStamp(t *testing.T) {
	var buf bytes.Buffer
	log := cog.New("test timestamp", cog.Options{
		Target:  cog.Writer(&buf),
		Level:   cog.InfoLevel,
		TimeKey: "time",
	})

	log.Info("testing timestamp", nil)

	m := map[string]interface{}{}
	err := json.Unmarshal(buf.Bytes(), &m)
	if err != nil {
		t.Error(err)
	}
	t.Log(m, buf.String())
	if int64(m["time"].(float64)) < 1600000000000000000 {
		t.Error("invalid timestamp")
	}
	if int64(m["time"].(float64)) > time.Now().UnixNano() {
		t.Error("invalid timestamp")
	}
}

func TestTimeStampBetterDebugNoKey(t *testing.T) {
	defer expect.Cleanup(t)
	var buf bytes.Buffer
	log := cog.New("test", cog.Options{
		Target:      cog.Writer(&buf),
		Level:       cog.DebugLevel,
		BetterDebug: true,
	})

	log.Debug("testing timestamp", nil)

	m := map[string]interface{}{}
	err := json.Unmarshal(buf.Bytes(), &m)
	expect.Nil(err)
	tm := time.Time{}
	err = (&tm).UnmarshalText([]byte(m["time"].(string)))
	expect.Nil(err)
	expect.If(tm.UnixNano() < 1600000000000000000, "invalid timestamp")
	expect.If(tm.UnixNano() > time.Now().UnixNano(), "invalid timestamp")
}

func TestTimeStampBetterDebug(t *testing.T) {
	defer expect.Cleanup(t)
	var buf bytes.Buffer
	log := cog.New("test", cog.Options{
		Target:      cog.Writer(&buf),
		Level:       cog.DebugLevel,
		TimeKey:     "time",
		BetterDebug: true,
	})

	log.Debug("testing timestamp", nil)

	m := map[string]interface{}{}
	err := json.Unmarshal(buf.Bytes(), &m)
	expect.Nil(err)
	tm := time.Time{}
	err = (&tm).UnmarshalText([]byte(m["time"].(string)))
	expect.Nil(err)
	expect.If(tm.UnixNano() < 1600000000000000000, "invalid timestamp")
	expect.If(tm.UnixNano() > time.Now().UnixNano(), "invalid timestamp")
}

func TestTimeStampFormatted(t *testing.T) {
	defer expect.Cleanup(t)
	var buf bytes.Buffer
	log := cog.New("test", cog.Options{
		Target:     cog.Writer(&buf),
		Level:      cog.DebugLevel,
		TimeKey:    "time",
		TimeFormat: time.RFC3339Nano,
	})

	log.Debug("testing timestamp")

	m := map[string]interface{}{}
	err := json.Unmarshal(buf.Bytes(), &m)
	expect.Nil(err)
	tm := time.Time{}
	expect.Nil((&tm).UnmarshalText([]byte(m["time"].(string))))
	expect.If(tm.UnixNano() < 1600000000000000000, "invalid timestamp")
	expect.If(tm.UnixNano() > time.Now().UnixNano(), "invalid timestamp")

	buf.Reset()
	log.Info("testing timestamp")
	m = map[string]interface{}{}
	err = json.Unmarshal(buf.Bytes(), &m)
	expect.Nil(err)
	expect.Nil((&tm).UnmarshalText([]byte(m["time"].(string))))
	expect.If(tm.UnixNano() < 1600000000000000000, "invalid timestamp")
	expect.If(tm.UnixNano() > time.Now().UnixNano(), "invalid timestamp")
}

func TestNew(t *testing.T) {
	defer expect.Cleanup(t)
	log := cog.New("test", cog.Options{
		Target: cog.Writer(ioutil.Discard),
	})
	expect.NotNil(log)
}

type badWriter struct{}

func (b badWriter) Write([]byte) (int, error) {
	//nolint:goerr113 // stub writer
	return 0, errors.New("voluntary error")
}

func TestWriteError(t *testing.T) {
	errored := false
	log := cog.New("test", cog.Options{
		Target: cog.Writer(badWriter{}),
		Level:  cog.DebugLevel,
		OnError: func(err error) {
			errored = true
		},
	})

	log.Info("should error", nil)
	expect.If(!errored, "Write error badly reported")
}

func TestWriteContext(t *testing.T) {
	defer expect.Cleanup(t)
	errored := false
	log := cog.New("test", cog.Options{
		Target: cog.Writer(badWriter{}),
		Level:  cog.DebugLevel,
		OnError: func(err error) {
			errored = true
		},
		Context: func(enc cog.Encoder) {
			enc.String("error", "error")
		},
	})

	log.Info("should error", nil)
	expect.If(!errored, "Write error badly reported")

	log.Debug("should error", nil)
	expect.If(!errored, "Write error badly reported")
}

func TestNewContext(t *testing.T) {
	log := cog.New("test", cog.Options{
		Target: cog.Writer(ioutil.Discard),
		Level:  cog.InfoLevel,
		Context: func(enc cog.Encoder) {
			enc.String("surprise", "42")
		},
	})

	log.Info("testing new context", nil)
}

func TestFields(t *testing.T) {
	log := cog.New("test", cog.Options{
		Target: cog.Writer(ioutil.Discard),
		Level:  cog.InfoLevel,
		Context: func(enc cog.Encoder) {
			enc.String("surprise", "42")
		},
	})

	log.Info("testing timestamp", func(enc cog.Encoder) {
		enc.Bool("works", true)
	})
}

type testWriter bool

func (t *testWriter) WrittenTo() bool {
	return bool(*t)
}
func (t *testWriter) Reset() {
	*t = false
}
func (t *testWriter) Write(b []byte) (n int, err error) {
	n = len(b)
	*t = true
	return
}

func TestCaller(t *testing.T) {
	defer expect.Cleanup(t)
	var buf bytes.Buffer
	log := cog.New("test", cog.Options{
		Target:    cog.Writer(&buf),
		Level:     cog.InfoLevel,
		CallerKey: "caller",
	})

	for i := 0; i < 12; i++ {
		log.Info("testing caller", nil)
		m := map[string]string{}

		err := json.Unmarshal(buf.Bytes(), &m)
		expect.Nil(err)
		if !strings.Contains(m["caller"], "cog_test.go") {
			t.Errorf("caller not properly returned: %s", m["caller"])
		}
		buf.Reset()
	}
}

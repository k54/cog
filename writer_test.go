package cog

import (
	"io"
	"os"
	"path"
	"sync"
	"testing"

	"gitlab.com/k54/expect"
)

type lockedWriterTest struct {
	sync.Mutex
}

func (l *lockedWriterTest) Write(b []byte) (int, error) {
	return len(b), nil
}

func TestWriterNil(t *testing.T) {
	defer expect.Cleanup(t)
	target := Writer(nil)
	expect.Nil(target)
}

func TestGetStderrout(t *testing.T) {
	defer expect.Cleanup(t)
	expect.NotNil(Stderr())
	expect.NotNil(Stdout())
}

func TestWriterNoopSync(t *testing.T) {
	defer expect.Cleanup(t)
	target := Writer(&lockedWriterTest{})
	expect.Nil(target.Sync())
	_, err := target.Write([]byte{1})
	target.Lock()
	expect.Nil(err)
	target.Unlock()
}

func TestWriterAlreadyTarget(t *testing.T) {
	defer expect.Cleanup(t)
	target := Writer(&lockedWriterTest{})
	target = Writer(target)
	expect.Nil(target.Sync())
	_, err := target.Write([]byte{1})
	target.Lock()
	expect.Nil(err)
	target.Unlock()
}

type nilWriter struct{}

func (w nilWriter) Write(b []byte) (int, error) {
	return len(b), nil
}

func TestWriterOnlyWriter(t *testing.T) {
	defer expect.Cleanup(t)
	target := Writer(nilWriter{})
	expect.Nil(target.Sync())
	_, err := target.Write([]byte{1})
	target.Lock()
	expect.Nil(err)
	target.Unlock()
}

func TestWriterOSFile(t *testing.T) {
	defer expect.Cleanup(t)
	file, err := os.CreateTemp("", "cog.test")
	expect.Nil(err)
	target := Writer(file)
	expect.Nil(target.Sync())
	_, err = target.Write([]byte{1})
	target.Lock()
	expect.Nil(err)
	target.Unlock()
}

func TestMustFileBad(t *testing.T) {
	old := osOpenFile
	osOpenFile = func(name string, flag int, perm os.FileMode) (*os.File, error) {
		return nil, io.EOF
	}
	defer func() {
		osOpenFile = old
	}()
	expect.Panic(func() {
		_ = MustFile("anything")
	})
}

func TestMustFile(t *testing.T) {
	defer expect.Cleanup(t)
	tmpDir := os.TempDir()
	target := MustFile(path.Join(tmpDir, "cog.test"))

	expect.Nil(target.Sync())
	_, err := target.Write([]byte{1})
	target.Lock()
	expect.Nil(err)
	target.Unlock()
}

.PHONY: cover-all
cover-all: cover-cog cover-notsafe cover-medea

lint:
	golangci-lint run --fix ./...

# ugly but avoids quite some copy-paste
define add_target
.PHONY: cover-$(1)
cover-$(1):
	@go test -coverprofile /tmp/gocover.out $(2)
	@go tool cover -func /tmp/gocover.out | { grep -v "100.0%" || :; } 
	@$(RM) /tmp/gocover.out
endef

$(eval $(call add_target,cog,.))
$(eval $(call add_target,notsafe,./pkg/notsafe))
$(eval $(call add_target,medea,./pkg/medea))

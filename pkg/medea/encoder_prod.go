package medea

import (
	"io"
	"sync"
)

// jsonEncoder is a JSON encoder, not safe for concurrent use.
// Use GetEncoder to have a safe and pooled instance
// Duplicate keys are NOT checked against
type JSONEncoder struct {
	Buf []byte
}

func (je *JSONEncoder) ReplaceLast(b byte) {
	if len(je.Buf) == 0 {
		je.AppendByte(b)
		return
	}
	if je.Buf[len(je.Buf)-1] == ',' {
		je.Buf[len(je.Buf)-1] = b
	} else {
		je.AppendByte(b)
	}
}
func (je *JSONEncoder) AppendByte(b byte) {
	je.Buf = append(je.Buf, b)
}
func (je *JSONEncoder) AppendString(s string) {
	je.Buf = append(je.Buf, s...)
}

func (je *JSONEncoder) Write(b []byte) (n int, err error) {
	n = len(b)
	je.Buf = append(je.Buf, b...)
	return
}

func (je *JSONEncoder) writeKey(key string) {
	if key == "" {
		return
	}
	je.AppendString(`"`)
	je.AppendString(key) // quite faster than writeEscapedString, and keys rarely have escape sequenes in them (error caught by debug encoder)
	je.AppendString(`":`)
}

func (je *JSONEncoder) Cache() []byte {
	return je.Buf
}

func (je *JSONEncoder) WriteCached(b []byte) {
	je.Buf = append(je.Buf, b...)
	return
}

func (je *JSONEncoder) WriteTo(w io.Writer) (n int64, err error) {
	if len(je.Buf) == 0 {
		je.AppendString("{}")
	} else {
		// remove last ','
		cur := len(je.Buf) - 1
		for je.Buf[cur] != ',' {
			cur--
		}
		copy(je.Buf[cur:], je.Buf[cur+1:])
		je.Buf[len(je.Buf)-1] = 0
		je.Buf = je.Buf[:len(je.Buf)-1]
	}
	n, err = writeAll(je.Buf, w)
	// reset buffer
	je.Buf = je.Buf[:0]
	return
}

func newEncoder() *JSONEncoder {
	return &JSONEncoder{
		Buf: make([]byte, 0, smallBufferSize),
	}
}

var encPool = sync.Pool{
	New: func() interface{} {
		return newEncoder()
	},
}

func (je *JSONEncoder) Release() { encPool.Put(je) }

package medea

import "encoding/base64"

func (je *JSONEncoder) Bytes(key string, values []byte) {
	je.Key(key)
	base64.StdEncoding.EncodeToString(values)

	je.AppendByte('"')
	enc := base64.StdEncoding
	buf := make([]byte, enc.EncodedLen(len(values)))
	enc.Encode(buf, values)
	je.Buf = append(je.Buf, buf...)
	je.AppendString(`",`)
}

func (je *JSONEncoder) IntSlice(key string, values []int) {
	je.ArrayFunc(key, func(enc Encoder) {
		for _, v := range values {
			enc.Int("", v)
		}
	})
}

func (je *JSONEncoder) BoolSlice(key string, values []bool) {
	je.ArrayFunc(key, func(enc Encoder) {
		for _, v := range values {
			enc.Bool("", v)
		}
	})
}

func (je *JSONEncoder) Int64Slice(key string, values []int64) {
	je.ArrayFunc(key, func(enc Encoder) {
		for _, v := range values {
			enc.Int64("", v)
		}
	})
}

func (je *JSONEncoder) Uint64Slice(key string, values []uint64) {
	je.ArrayFunc(key, func(enc Encoder) {
		for _, v := range values {
			enc.Uint64("", v)
		}
	})
}

func (je *JSONEncoder) FloatSlice(key string, values []float64) {
	je.ArrayFunc(key, func(enc Encoder) {
		for _, v := range values {
			enc.Float("", v)
		}
	})
}

func (je *JSONEncoder) StringSlice(key string, values []string) {
	je.ArrayFunc(key, func(enc Encoder) {
		for _, v := range values {
			enc.String("", v)
		}
	})
}

package medea

import (
	"fmt"
	"io"
	"time"
)

func valueOf(je Encoder, key string, value interface{}) (ok bool) {
	ok = true
	switch v := value.(type) {
	case nil:
		je.Nil(key)
	case fmt.Stringer:
		je.String(key, v.String())
	case ObjectExporter:
		je.Object(key, v)
	case ArrayExporter:
		je.Array(key, v)
	case time.Time:
		je.Time(key, v)

	case string:
		je.String(key, v)
	case error:
		je.Err(key, v)
	case bool:
		je.Bool(key, v)

	case int:
		je.Int(key, v)
	case int8:
		je.Int64(key, int64(v))
	case int16:
		je.Int64(key, int64(v))
	case int32:
		je.Int64(key, int64(v))
	case int64:
		je.Int64(key, v)

	case float32:
		je.Float(key, float64(v))
	case float64:
		je.Float(key, v)

	case uint8:
		je.Uint64(key, uint64(v))
	case uint16:
		je.Uint64(key, uint64(v))
	case uint32:
		je.Uint64(key, uint64(v))
	case uint64:
		je.Uint64(key, v)
	case uintptr:
		je.Uint64(key, uint64(v))

	case []byte:
		je.Bytes(key, v)
	case []int:
		je.IntSlice(key, v)
	case []int64:
		je.Int64Slice(key, v)
	case []uint64:
		je.Uint64Slice(key, v)
	case []float64:
		je.FloatSlice(key, v)
	case []bool:
		je.BoolSlice(key, v)
	case []string:
		je.StringSlice(key, v)

	case map[string]interface{}:
		je.ObjectFunc(key, func(enc Encoder) {
			for k, v := range v {
				enc.Value(k, v)
			}
		})
	case map[string]bool:
		je.ObjectFunc(key, func(enc Encoder) {
			for k, v := range v {
				enc.Bool(k, v)
			}
		})
	case map[string]string:
		je.ObjectFunc(key, func(enc Encoder) {
			for k, v := range v {
				enc.String(k, v)
			}
		})
	default:
		ok = false
	}
	return
}

func writeAll(buf []byte, w io.Writer) (n int64, err error) {
	if len(buf) == 0 {
	}
	if nBytes := len(buf); nBytes > 0 {
		m, e := w.Write(buf)
		if m > nBytes {
			panic("medea.Encoder.WriteTo: invalid Write count")
		}

		n = int64(m)
		if e != nil {
			return n, e
		}

		// all bytes should have been written, by definition of
		// Write method in io.Writer
		if m != nBytes {
			return n, io.ErrShortWrite
		}
	}

	return n, nil
}
func writeEscapedString(b []byte, s string) []byte {
	n := 0
	for i, c := range []byte(s) {
		if c >= 0x20 && c != '"' && c != '\\' {
			continue
		}
		b = append(b, s[n:i]...)
		// dont write byte unescaped
		n = i + 1
		// common cases
		switch c {
		case '"':
			b = append(b, `\"`...)
		case '\\':
			b = append(b, `\\`...)
		case '\n':
			b = append(b, `\n`...)
		case '\r':
			b = append(b, `\r`...)
		case '\t':
			b = append(b, `\t`...)
		default:
			// escape chars
			b = append(b, '\\', 'u', '0', '0', hex[c>>4], hex[c&0xF])
		}
	}
	b = append(b, s[n:]...)
	return b
}

// taken and adapted from strconv
// slightly slower for 0 < i < 100
// faster for every other value

func appendInt(dst []byte, i int64) []byte {
	dst = formatBits(dst, uint64(i), i < 0)
	return dst
}
func appendUint(dst []byte, i uint64) []byte {
	dst = formatBits(dst, i, false)
	return dst
}

const hex = "0123456789abcdef"

const smallsString = "00010203040506070809" +
	"10111213141516171819" +
	"20212223242526272829" +
	"30313233343536373839" +
	"40414243444546474849" +
	"50515253545556575859" +
	"60616263646566676869" +
	"70717273747576777879" +
	"80818283848586878889" +
	"90919293949596979899"

const host32bit = ^uint(0)>>32 == 0

func formatBits(dst []byte, u uint64, neg bool) (d []byte) {

	var a [32]byte // 20 should be sufficient, 32 just in case
	i := len(a)
	if neg {
		u = -u
	}

	// convert bits
	// We use uint values where we can because those will
	// fit into a single register even on a 32bit machine.

	// common case: use constants for / because
	// the compiler can optimize it into a multiply+shift

	if host32bit {
		// convert the lower digits using 32bit operations
		for u >= 1e9 {
			// Avoid using r = a%b in addition to q = a/b
			// since 64bit division and modulo operations
			// are calculated by runtime functions on 32bit machines.
			q := u / 1e9
			us := uint(u - q*1e9) // u % 1e9 fits into a uint
			for j := 4; j > 0; j-- {
				is := us % 100 * 2
				us /= 100
				i -= 2
				a[i+1] = smallsString[is+1]
				a[i+0] = smallsString[is+0]
			}

			// us < 10, since it contains the last digit
			// from the initial 9-digit us.
			i--
			a[i] = smallsString[us*2+1]
			u = q
		}
		// u < 1e9
	}

	// u guaranteed to fit into a uint
	us := uint(u)
	for us >= 100 {
		is := us % 100 * 2
		us /= 100
		i -= 2
		a[i+1] = smallsString[is+1]
		a[i+0] = smallsString[is+0]
	}
	// us < 100
	is := us * 2
	i--
	a[i] = smallsString[is+1]
	if us >= 10 {
		i--
		a[i] = smallsString[is]
	}

	// add sign, if any
	if neg {
		i--
		a[i] = '-'
	}

	d = append(dst, a[i:]...)
	return
}

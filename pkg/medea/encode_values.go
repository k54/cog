package medea

import (
	"fmt"
	"strconv"
	"time"
)

func (je *JSONEncoder) Value(key string, v interface{}) {
	ok := valueOf(je, key, v)
	if !ok {
		je.String(key, fmt.Sprint(v))
	}
}

func (je *JSONEncoder) StringBytes(key string, value []byte) {
	je.writeKey(key)
	je.AppendByte('"')
	je.Buf = append(je.Buf, value...)
	je.AppendString(`",`)
}

func (je *JSONEncoder) RawString(key, value string) {
	je.writeKey(key)
	je.AppendByte('"')
	je.AppendString(value)
	je.AppendString(`",`)
}

func (je *JSONEncoder) String(key, value string) {
	je.writeKey(key)
	je.AppendByte('"')
	je.Buf = writeEscapedString(je.Buf, value)
	je.AppendString(`",`)
}

func (je *JSONEncoder) Err(key string, value error) {
	je.String(key, value.Error())
}

func (je *JSONEncoder) Stringer(key string, value fmt.Stringer) {
	je.String(key, value.String())
}

func (je *JSONEncoder) Float(key string, value float64) {
	je.writeKey(key)
	je.Buf = strconv.AppendFloat(je.Buf, value, 'f', -1, 64)
	je.AppendByte(',')
}

func (je *JSONEncoder) Nil(key string) {
	je.writeKey(key)
	je.AppendString("null,")
}

func (je *JSONEncoder) Bool(key string, value bool) {
	je.writeKey(key)
	if value {
		je.AppendString("true,")
	} else {
		je.AppendString("false,")
	}
}

func (je *JSONEncoder) Key(key string) {
	je.writeKey(key)
}

func (je *JSONEncoder) ArrayFunc(key string, value func(Encoder)) {
	je.writeKey(key)
	je.AppendByte('[')
	value(je)
	je.ReplaceLast(']')
	je.AppendByte(',')
}

func (je *JSONEncoder) ObjectFunc(key string, value func(Encoder)) {
	je.writeKey(key)
	je.AppendByte('{')
	value(je)
	je.ReplaceLast('}')
	je.AppendByte(',')
}

func (je *JSONEncoder) Array(key string, value ArrayExporter) {
	if value.IsNil() {
		return
	}
	je.ArrayFunc(key, value.ExportArray)
}

func (je *JSONEncoder) Object(key string, value ObjectExporter) {
	if value.IsNil() {
		return
	}
	je.ObjectFunc(key, value.ExportObject)
}

func (je *JSONEncoder) Int(key string, value int) {
	je.Int64(key, int64(value))
}

func (je *JSONEncoder) Time(key string, value time.Time) {
	je.Int64(key, value.Unix())
}

func (je *JSONEncoder) Int64(key string, value int64) {
	je.writeKey(key)
	je.Buf = appendInt(je.Buf, value)
	je.AppendByte(',')
}

func (je *JSONEncoder) Uint64(key string, value uint64) {
	je.writeKey(key)
	je.Buf = appendUint(je.Buf, value)
	je.AppendByte(',')
}

// Package medea implements fast and easy json serialization for all
//
// This library is optimized for 64-bit architectures and may not work on 32-bit
package medea

import (
	"fmt"
	"io"
	"time"
)

// Encoder represents an object that can encode objects to a serialized representation, for example JSON
// For serialization, an empty key means no key (in an array for example)
// Keys are not escaped at all, use caution when creating them
// This interface only contains the safest functions to call, for exposing an API for example
// Most if not all implementations should also implement ControlledEncoder
type Encoder interface {
	// Value encodes value as a valid sub value
	// It is quite slow as it doesnt know the type at compile time
	// Useful for debugging
	Value(key string, value interface{})

	String(key, value string)                // String escapes only what is mandated by corresponding encoding, i.e. RFC 8259 for JSON
	RawString(key, value string)             // CAUTION: RawString has no escaping at all, not even quotes for JSON. Use String in most cases (RawString is faster though)
	Stringer(key string, value fmt.Stringer) // Alias of String(key, value.String())
	Err(key string, value error)             // Alias of String(key, value.Error())
	StringBytes(key string, value []byte)    // Alias of RawString(key, string(value))

	Nil(key string)                   // Nil encodes absence of a value, i.e. JSON token `null`
	Bool(key string, value bool)      // Bool encodes value as a boolean, i.e. JSON token `true` or `false`
	Time(key string, value time.Time) // Encodes the time as a UNIX timestamp (second)

	// Bytes writes values as a blob, for example as a base64 string in JSON
	Bytes(key string, values []byte)

	// Encodes value as a number
	Int(key string, value int)
	Int64(key string, value int64)
	Uint64(key string, value uint64)
	Float(key string, value float64) // Float is ~3x slower than Int64

	// Encodes values as an array of the contents
	IntSlice(key string, values []int)
	BoolSlice(key string, values []bool)
	Int64Slice(key string, values []int64)
	Uint64Slice(key string, values []uint64)
	FloatSlice(key string, values []float64)
	StringSlice(key string, values []string)

	Array(key string, value ArrayExporter)      // Array encases value in an array
	ArrayFunc(key string, value func(Encoder))  // ArrayFunc encases value in an array
	Object(key string, jo ObjectExporter)       // Object encases value in an object
	ObjectFunc(key string, value func(Encoder)) // ObjectFunc encases value in an object

	// A map is easy enough to encode:
	// var myMap map[string]int64
	// ...
	// enc.ObjectFunc("my_map", func(enc Encoder) {
	// 	for k, v := range myMap {
	// 		enc.Int64(k, v)
	// 	}
	// })
	//
	// As are custom slices:
	// var mySlice []int16
	// ...
	// enc.ArrayFunc("my_slice", func(enc Encoder) {
	// 	for _, v := range mySlice {
	// 		enc.Int64("", int64(v))
	// 	}
	// })
}

// ControlledEncoder represents an object that can encode objects to a serialized representation, for example JSON
// For serialization, an empty key means no key (in an array for example)
// This interface is meant for developers to create libraries with
// Only use if you really know what you're getting into (don't expose to users)
type ControlledEncoder interface {
	Encoder

	// WriteTo writes the buffer to w
	// Will only return an error on invalid w.Write() call
	// Should also reset the internal Encoder state to be used again
	io.WriterTo

	// Releases the ControlledEncoder to an internal pool
	// Don't forget to call when finished with the object, but only once
	Release()

	// Cache returns the internal(!) buffer of the Encoder to call WriteCached with
	// DO NOT Release() afterwards if you want to keep the byte slice
	Cache() []byte
	// WriteCached appends the parameter t the internal(!) buffer
	// Not safe at all to use, but very useful
	WriteCached([]byte)
}

var _ ControlledEncoder = &JSONEncoder{}

// var _ ControlledEncoder = &medeaEncoder{}

// encoder -> exporter

// ObjectExporter represents an object that can be marshalled
type ObjectExporter interface {
	ExportObject(enc Encoder)
	IsNil() bool
}

// ArrayExporter represents an object that can be marshalled
type ArrayExporter interface {
	ExportArray(enc Encoder)
	IsNil() bool
}

// GetEncoder borrows a *JSONEncoder from the pool.
// Do not forget to Release() it.
func GetEncoder() *JSONEncoder {
	enc := encPool.Get().(*JSONEncoder)
	enc.Buf = enc.Buf[:0]
	return enc
}

// MarshalTo writes a JSON representation of jo to w
func MarshalTo(w io.Writer, jo ObjectExporter) (err error) {
	enc := GetEncoder()
	enc.Object("", jo)
	_, err = enc.WriteTo(w)
	enc.Release()
	return
}

// MarshalArrayTo writes a JSON representation of ja to w
func MarshalArrayTo(w io.Writer, ja ArrayExporter) (err error) {
	enc := GetEncoder()
	enc.Array("", ja)
	_, err = enc.WriteTo(w)
	enc.Release()
	return
}

const smallBufferSize = 512

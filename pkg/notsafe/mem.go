package notsafe

import (
	"reflect"
	"unsafe"
)

func String(b []byte) string {
	// (*bytes.Buffer).String() does that
	//nolint:gosec // not the most correct code, but works for now
	return *(*string)(unsafe.Pointer(&b))
}

func Bytes(s string) []byte {
	//nolint:gosec // audited unsafe.Pointer use
	return (*[0x7fff0000]byte)(
		unsafe.Pointer(
			(*reflect.StringHeader)(unsafe.Pointer(&s)).Data,
		))[:len(s):len(s)]
}

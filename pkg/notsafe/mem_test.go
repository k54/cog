//nolint:ifshort // testing package
package notsafe

import "testing"

func TestStringBytes(t *testing.T) {
	str := "never gonna give you up, ..."
	str2 := String(Bytes(str))
	if str != str2 {
		t.Fatal("String(Bytes(str)) != str")
	}
}

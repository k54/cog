package notsafe

import (
	"fmt"
	"runtime"
	"sync"
)

var callersCache = sync.Map{}

// The argument skip is the number of stack frames to skip before recording in pc,
// with 0 identifying the caller of CallerName.
// It returns a formatter caller name, such as `package.func (file:line)`
//
// Example: `gitlab.com/k54/cog.TestCaller (cog_test.go:42)`
//
// You may not make any assumption about the format, it could change anytime without any warning
//
// Return value is cached, so this function is ~3x faster than manual calls (and more correct
// because it accounts for inlined functions, contrary to the common runtime.FuncForPC)
//
// It's so fast, a cache miss can actually be still faster than doing it by hand (see benchmarks).
func CallerName(skip int) string {

	// rpc escapes to heap, but it's still faster than using a sync.Pool
	rpc := make([]uintptr, 1)

	// skip + 1 for Callers semantics. +1 for CallerName skipping
	const skipFrames = 2

	runtime.Callers(skip+skipFrames, rpc)
	pc := rpc[0]

	// cache hit
	if v, ok := callersCache.Load(pc); ok {
		return v.(string)
	}

	// more is always false, we have 1 frame
	frame, _ := runtime.CallersFrames(rpc).Next()

	// no issue using slow fmt, it's cached either way
	// fmt is more readable here
	str := fmt.Sprintf("%s (%s:%d)", frame.Function, frame.File, frame.Line)

	callersCache.Store(pc, str)
	return str
}

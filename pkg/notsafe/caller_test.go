package notsafe

import (
	"fmt"
	"runtime"
	"strings"
	"testing"
)

func TestCallerName(t *testing.T) {
	// 2nd-12th calls cached
	for i := 0; i < 12; i++ {
		name := CallerName(0)

		if !strings.Contains(name, "notsafe.TestCallerName") {
			t.Fatal("CallerName needs to return package name and func name")
		}
		if !strings.Contains(name, "caller_test.go") {
			t.Fatal("CallerName needs to return file name")
		}
	}
}

func BenchmarkCallerName(b *testing.B) {
	for i := 0; i < b.N; i++ {
		name := CallerName(0)
		_ = name
	}
}

// copy-paste of CallerName with cache store commented out.
func callerNameCacheMiss(skip int) string {
	// rpc escapes to heap, but it's still faster than using a sync.Pool
	rpc := make([]uintptr, 1)

	// skip + 1 for Callers semantics. +1 for CallerName skipping
	runtime.Callers(skip+2, rpc)
	pc := rpc[0]

	// cache hit
	if v, ok := callersCache.Load(pc); ok {
		return v.(string)
	}

	// more is always false, we have 1 frame
	frame, _ := runtime.CallersFrames(rpc).Next()

	// no issue using slow fmt, it's cached either way
	// fmt is more readable here
	str := fmt.Sprintf("%s (%s:%d)", frame.Function, frame.File, frame.Line)

	// callersCache.Store(pc, str)
	return str
}

func BenchmarkCallerNameCacheMiss(b *testing.B) {
	for i := 0; i < b.N; i++ {
		name := callerNameCacheMiss(0)
		_ = name
	}
}

func BenchmarkCallerNameManualCorrect(b *testing.B) {
	for i := 0; i < b.N; i++ {
		// correct way to do it
		rpc := make([]uintptr, 1)
		n := runtime.Callers(1, rpc)
		if n < 1 {
			return
		}
		frame, _ := runtime.CallersFrames(rpc).Next()
		_ = fmt.Sprintf("%s (%s:%d)", frame.Function, frame.File, frame.Line)
	}
}

func BenchmarkCallerNameManual(b *testing.B) {
	for i := 0; i < b.N; i++ {
		// commonly seen way to do it, not correct for inlined functions
		pc, _, _, _ := runtime.Caller(0)
		fn := runtime.FuncForPC(pc)
		file, line := fn.FileLine(pc)
		_ = fmt.Sprintf("%s (%s:%d)", fn.Name(), file, line)
	}
}
